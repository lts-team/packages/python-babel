#!/bin/sh

set -e

dir_to_symlink_postrm() {
	local PATHNAME="$1"
	local SYMLINK_TARGET="$2"
	local LASTVERSION="$3"
	local PACKAGE="$4"

	while [ "$1" != "--" -a $# -gt 0 ]; do shift; done
	shift

	if [ "$1" = "purge" ] && [ -d "${PATHNAME}.dpkg-backup" ]; then
		rm -rf "${PATHNAME}.dpkg-backup"
	fi
	if [ "$1" = "abort-install" -o "$1" = "abort-upgrade" ] && [ -n "$2" ] && [ -d "${PATHNAME}.dpkg-backup" ] &&
	   [ \( ! -h "$PATHNAME" -a -d "$PATHNAME" -a -f "$PATHNAME/.dpkg-staging-dir" \) -o \( -h "$PATHNAME" -a "$(readlink -f $PATHNAME)" = "$SYMLINK_TARGET" \) ] &&
	   dpkg --compare-versions "$2" le-nl "$LASTVERSION"; then
		if [ -h "$PATHNAME" ]; then
			rm -f "$PATHNAME"
		else
			# The staging directory must be empty, as no other package
			# should have been unpacked inbetween.
			rm -f "$PATHNAME/.dpkg-staging-dir"
			rmdir "$PATHNAME"
		fi
	fi
}

if [ "$1" = "remove" ] || [ "$1" = "disappear" ]; then
	update-alternatives --remove pybabel /usr/bin/pybabel-python2
fi

if [ -e /usr/lib/python2.6/dist-packages/babel ] ; then
	dir_to_symlink_postrm /usr/lib/python2.6/dist-packages/babel/localedata ../../../../share/python-babel-localedata/localedata 1.3+dfsg.1-2.1~ python-babel -- "$@"
fi
dir_to_symlink_postrm /usr/lib/python2.7/dist-packages/babel/localedata ../../../../share/python-babel-localedata/localedata 1.3+dfsg.1-2.1~ python-babel -- "$@"

#DEBHELPER#

exit 0
