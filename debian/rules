#!/usr/bin/make -f

PYTHONS:=$(shell pyversions -vr)
PYTHON3S:=$(shell py3versions -vr)

%:
	dh $@ --buildsystem=python_distutils --with python2,python3,sphinxdoc

override_dh_auto_build:
	dh_auto_build -O--buildsystem=python_distutils
	# Generate the localedata folder data out of the xml files
	# downloaded in debian/repack
	scripts/import_cldr.py common

override_dh_clean:
	dh_clean -O--buildsystem=python_distutils
	rm -rf babel/localedata/*.dat babel/global.dat

override_dh_auto_install:
	set -e && for pyvers in $(PYTHONS); do \
		python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python-babel; \
	done
	set -e && for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install --install-layout=deb \
			--root $(CURDIR)/debian/python3-babel; \
	done
	mv debian/python-babel/usr/bin/pybabel debian/python-babel/usr/bin/pybabel-python2
	mv debian/python3-babel/usr/bin/pybabel debian/python3-babel/usr/bin/pybabel-python3

override_dh_python2:
	dh_python2 -O--buildsystem=python_distutils
	set -e && for pyvers in $(PYTHONS); do \
		rm debian/python-babel/usr/lib/python$$pyvers/dist-packages/babel/localedata/*.dat ; \
		rmdir debian/python-babel/usr/lib/python$$pyvers/dist-packages/babel/localedata ; \
		ln -s ../../../../share/python-babel-localedata/localedata debian/python-babel/usr/lib/python$$pyvers/dist-packages/babel/localedata ; \
		rm debian/python-babel/usr/lib/python$$pyvers/dist-packages/babel/global.dat ; \
		ln -s ../../../../share/python-babel-localedata/global.dat debian/python-babel/usr/lib/python$$pyvers/dist-packages/babel/global.dat ; \
	done
	# Carefully remove an eventual /usr/share/pyshared/babel/localedata
	# This doesn't exist in Sid / Jessie anymore, but it is still needed
	# for backporting to Wheezy. Adding tests and -f fixes #751056 (eg: FTBFS)
	set -e && if [ -d debian/python-babel/usr/share/pyshared/babel/localedata ] ; then \
		rm -f debian/python-babel/usr/share/pyshared/babel/localedata/*.dat ; \
		rmdir debian/python-babel/usr/share/pyshared/babel/localedata ; \
		rm -f debian/python-babel/usr/share/pyshared/babel/global.dat ; \
	fi

override_dh_python3:
	dh_python3 -O--buildsystem=python_distutils
	rm -r debian/python3-babel/usr/lib/python3/dist-packages/babel/localedata ; \
	ln -s ../../../../share/python-babel-localedata/localedata debian/python3-babel/usr/lib/python3/dist-packages/babel/localedata ; \
	rm -r debian/python3-babel/usr/lib/python3/dist-packages/babel/global.dat ; \
	ln -s ../../../../share/python-babel-localedata/global.dat debian/python3-babel/usr/lib/python3/dist-packages/babel/global.dat ; \

override_dh_installdocs:
	sphinx-build docs docs/_build
	dh_installdocs -O--buildsystem=python_distutils
	mkdir -p debian/python-babel-doc/usr/share/doc/python-babel-doc/html
	cp -auxf docs/_build/* debian/python-babel-doc/usr/share/doc/python-babel-doc/html

override_dh_sphinxdoc:
	dh_sphinxdoc -O--buildsystem=python_distutils
	rm debian/python-babel-doc/usr/share/doc/python-babel-doc/html/_sources/license.txt

override_dh_auto_clean:
	rm -rf docs/_build build Babel.egg-info
	dh_auto_clean -O--buildsystem=python_distutils

get-orig-source:
	uscan --noconf --force-download --rename --download-current-version

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	LC_ALL=C py.test
	LC_ALL=C py.test-3
endif
